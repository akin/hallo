cmake_minimum_required(VERSION 3.11)
project(main C CXX)

# Project stuffs
add_executable(${PROJECT_NAME})

set(CMAKE_MODULE_PATH 
	${CMAKE_MODULE_PATH} 
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake"
)

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/*.c
	${CMAKE_CURRENT_LIST_DIR}/*.inl
	${CMAKE_CURRENT_LIST_DIR}/*.h
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_SOURCES}
	)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/private"
)

target_link_libraries(
    ${PROJECT_NAME} 
	PUBLIC
		stb
		glfw
		glm
		glad
)

## features
target_compile_features(
	${PROJECT_NAME} 
	PRIVATE cxx_std_14
)

## Add system information
cmake_host_system_information(RESULT build_hostname QUERY HOSTNAME)
target_compile_definitions(
	${PROJECT_NAME} 
	PRIVATE 
		BUILD_SYSTEM_HOSTNAME=\"${build_hostname}\"
		BUILD_SYSTEM_VERSION=\"${CMAKE_SYSTEM_VERSION}\"
		BUILD_SYSTEM_NAME=\"${CMAKE_SYSTEM_NAME}\"
)
